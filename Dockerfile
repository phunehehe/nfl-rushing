# Build the release
FROM elixir:1.9.1-alpine AS build

COPY assets assets
COPY config config
COPY lib lib
COPY mix.exs mix.lock ./
COPY priv priv

ENV MIX_ENV=prod
RUN mix local.hex --force
RUN mix local.rebar --force
RUN mix deps.get --only prod

RUN apk add nodejs-npm
RUN cd assets && npm install && npm run deploy
RUN mix phx.digest

RUN mix release

# Build the image
FROM alpine:3.11.3
RUN apk add --no-cache ncurses-libs
COPY --from=build /_build/prod/rel/nfl_rushing app
CMD /app/bin/nfl_rushing start
