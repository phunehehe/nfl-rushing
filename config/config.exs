# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

# Configures the endpoint
config :nfl_rushing, NflRushingWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "MbT3Xcn0wEmcKQj6kWdQ5C+R7ObntlY90BLSOIBzE8ItQcFrYlXb34Zk9QkcWegy",
  render_errors: [view: NflRushingWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: NflRushing.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: "m2VmJCTRbBINPz671/DffPcf/4v6dLw7"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
