let
  pkgs = import <nixpkgs> {};
  inherit (pkgs) lib stdenv;

in stdenv.mkDerivation rec {
  name = "nfl-rushing";

  buildInputs = [
    pkgs.elixir
    pkgs.nodejs
  ];

  installPhase = ''
    mkdir $out
    ${lib.concatMapStringsSep "\n" (i: "ln -s ${i} $out/") buildInputs}
  '';

  phases = ["installPhase"];

  LC_ALL = "C.UTF8";
}
