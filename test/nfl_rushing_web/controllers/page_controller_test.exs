defmodule NflRushingWeb.PageControllerTest do
  use NflRushingWeb.ConnCase

  defp find_indices(enumerable, values) do
    Enum.map(values, fn value ->
      Enum.find_index(enumerable, &(&1 == value))
    end)
  end

  defp decode(resp_body) do
    {:ok, stream} = StringIO.open(resp_body)

    stream
    |> IO.binstream(:line)
    |> CSV.decode!(headers: true)
  end

  defp sorted?(enumerable) do
    enumerable == Enum.sort(enumerable)
  end

  test "offers CSV download", %{conn: conn} do
    conn = get(conn, "/download", sort_key: "Pos", sort_direction: "desc", name_filter: "aa")

    assert response_content_type(conn, :csv)
    [header] = conn |> get_resp_header("content-disposition")
    assert String.match?(header, ~r/^attachment; filename=.+\.csv$/)

    assert conn.resp_body |> decode |> Enum.map(& &1["Player"]) == [
             "Jamaal Charles",
             "Aaron Rodgers",
             "Aaron Ripkowski"
           ]
  end

  test "sorts using natural order", %{conn: conn} do
    assert conn
           |> get("/download", sort_key: "Lng", name_filter: "do")
           |> Map.get(:resp_body)
           |> decode
           |> Enum.map(& &1["Lng"])
           |> find_indices(~w[-2 1 1T 4 16])
           |> sorted?

    assert conn
           |> get("/download", sort_key: "Yds", name_filter: "ez")
           |> Map.get(:resp_body)
           |> decode
           |> Enum.map(& &1["Yds"]) == ~w[-2 2 1,631]
  end

  test "ignores invalid parameters", %{conn: conn} do
    assert conn
           |> get("/download", sort_key: "Pos", sort_direction: "invalid", name_filter: "aa")
           |> Map.get(:resp_body)
           |> decode
           |> Enum.map(& &1["Player"]) == [
             "Aaron Ripkowski",
             "Aaron Rodgers",
             "Jamaal Charles"
           ]

    assert conn
           |> get("/download", sort_key: "invalid", invalid: "invalid")
           |> Map.get(:resp_body)
           |> decode
           |> Enum.to_list()
           |> length == 326
  end
end
