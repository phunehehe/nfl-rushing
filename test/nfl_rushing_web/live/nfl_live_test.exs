defmodule NflRushingWeb.NflLiveTest do
  # This test suite relies on existing data in rushing.json.
  # In more a more realistic setup it should manage test data by itself.

  use NflRushingWeb.ConnCase

  defp get_column(html, index) do
    html
    |> Floki.parse_document!()
    |> Floki.find("tr")
    |> Enum.drop(1)
    |> Enum.map(&(&1 |> Floki.find("td") |> Enum.at(index) |> elem(2) |> hd))
  end

  defp get_names(html) do
    get_column(html, 0)
  end

  defp get_top(html) do
    # This is a little wasteful because it didn't need to get a list of all names,
    # but that's not too bad for a test helper
    html |> get_names |> hd
  end

  setup do
    {:ok, view, html} = live(conn, "/")
    {:ok, view: view, html: html}
  end

  test "toggles between sort directions", %{view: view, html: html} do
    assert html |> get_top == "Aaron Ripkowski"
    assert view |> render_click(:sort, %{"key" => "TD"}) |> get_top == "Joe Banyard"
    assert view |> render_click(:sort, %{"key" => "TD"}) |> get_top == "LeGarrette Blount"
  end

  test "resets to first page when changing filter", %{view: view, html: html} do
    assert html |> get_top == "Aaron Ripkowski"
    assert view |> render_click(:next) |> get_top == "Ameer Abdullah"
    assert view |> render_change(:filter, %{"name" => "y"}) |> get_top == "Andy Dalton"
  end

  test "prevents pagination from going out of bounds", %{view: view} do
    assert view |> render_change(:filter, %{"name" => "z"}) |> get_top == "Bradley Marquez"
    names = view |> render_click(:next) |> get_names
    assert length(names) == 3
    assert hd(names) == "Scott Tolzien"
    names = view |> render_click(:next) |> get_names
    assert length(names) == 3
    assert hd(names) == "Scott Tolzien"
    names = view |> render_click(:previous) |> get_names
    assert length(names) == 12
    assert hd(names) == "Bradley Marquez"
    names = view |> render_click(:previous) |> get_names
    assert length(names) == 12
    assert hd(names) == "Bradley Marquez"
  end
end
