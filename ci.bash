#!/usr/bin/env bash
set -efuxo pipefail

export MIX_ENV=test

mix local.hex --force
mix local.rebar --force
mix deps.get

mix format --check-formatted
mix compile --warnings-as-errors
mix credo --strict
mix sobelow --config
mix coveralls.html
