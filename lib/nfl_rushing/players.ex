defmodule NflRushing.Players do
  @moduledoc """
  The Players context.
  """

  @doc """
  These are the attributes available for each player.

  Each attribute has a short key and a descriptive name.
  """
  def attributes do
    [
      {"Player", "Player's name"},
      {"Team", "Player's team abreviation"},
      {"Pos", "Player's postion"},
      {"Att/G", "Rushing Attempts Per Game Average"},
      {"Att", "Rushing Attempts"},
      {"Yds", "Total Rushing Yards"},
      {"Avg", "Rushing Average Yards Per Attempt"},
      {"Yds/G", "Rushing Yards Per Game"},
      {"TD", "Total Rushing Touchdowns"},
      {"Lng", "Longest Rush – a T represents a touchdown occurred"},
      {"1st", "Rushing First Downs"},
      {"1st%", "Rushing First Down Percentage"},
      {"20+", "Rushing 20+ Yards Each"},
      {"40+", "Rushing 40+ Yards Each"},
      {"FUM", "Rushing Fumbles"}
    ]
  end

  @doc """
  Returns the list of players.

  ## Examples

      iex> list_players()
      [%Player{}, ...]

  """
  # sobelow_skip ["Traversal.FileModule"]
  def list_players(sort_key, sort_direction, name_filter, limit \\ nil, offset \\ nil) do
    sort_key =
      if Enum.find(attributes(), fn {key, _name} -> key == sort_key end) do
        sort_key
      else
        nil
      end

    name_filter =
      if is_binary(name_filter) do
        name_filter
      else
        ""
      end

    :nfl_rushing
    |> Application.app_dir("priv/rushing.json")
    |> File.read!()
    |> Jason.decode!()
    |> filter_by_name(name_filter)
    |> sort(sort_key, sort_direction)
    |> drop(offset)
    |> take(limit)
  end

  defp sort(players, nil, _) do
    players
  end

  defp sort(players, sort_key, sort_direction) do
    # This can go away when upgrading to Elixir 1.10
    sorter =
      if sort_direction == :asc do
        &<=/2
      else
        &>=/2
      end

    Enum.sort_by(players, &natural_key(&1[sort_key]), sorter)
  end

  # Poor man's natural sort
  defp natural_key(value) do
    value
    |> is_binary
    |> if(do: String.replace(value, ",", ""), else: "#{value}")
    |> Float.parse()
    |> case do
      :error -> value
      result -> result
    end
  end

  defp drop(players, nil) do
    players
  end

  defp drop(players, offset) do
    Enum.drop(players, offset)
  end

  defp take(players, nil) do
    players
  end

  defp take(players, limit) do
    Enum.take(players, limit)
  end

  defp filter_by_name(players, "") do
    players
  end

  defp filter_by_name(players, key) do
    key = String.downcase(key)
    Enum.filter(players, &(&1 |> Map.get("Player") |> String.downcase() |> String.contains?(key)))
  end
end
