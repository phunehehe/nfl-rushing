defmodule NflRushingWeb.PageController do
  use NflRushingWeb, :controller
  alias NflRushing.Players

  def download(conn, params) do
    attributes = Players.attributes() |> Enum.map(fn {key, _name} -> key end)

    sort_direction =
      if params["sort_direction"] == "desc" do
        :desc
      else
        :asc
      end

    contents =
      Players.list_players(params["sort_key"], sort_direction, params["name_filter"])
      |> Enum.map(fn player -> Enum.map(attributes, &player[&1]) end)

    contents =
      [attributes | contents]
      |> CSV.encode()
      |> Enum.to_list()
      |> to_string

    conn
    |> put_resp_content_type("text/csv")
    |> put_resp_header("content-disposition", ~s(attachment; filename=nfl-rushing.csv))
    |> send_resp(200, contents)
  end
end
