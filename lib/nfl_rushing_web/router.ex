defmodule NflRushingWeb.Router do
  use NflRushingWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug Phoenix.LiveView.Flash
    plug :protect_from_forgery

    plug :put_secure_browser_headers, %{
      "content-security-policy" =>
        "default-src 'self' 'unsafe-eval'; style-src 'self' 'unsafe-inline'"
    }
  end

  scope "/", NflRushingWeb do
    pipe_through :browser
    live "/", NflLive
    get "/download", PageController, :download
  end
end
