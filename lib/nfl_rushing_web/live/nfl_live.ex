defmodule NflRushingWeb.NflLive do
  @moduledoc """
  Live table showing player statistics
  """

  use Phoenix.LiveView

  alias NflRushing.Players
  alias NflRushingWeb.Endpoint
  alias NflRushingWeb.Router.Helpers, as: Routes

  @page_size 12

  def render(assigns) do
    attributes = Players.attributes()

    download_path =
      Routes.page_path(
        Endpoint,
        :download,
        Map.take(assigns.socket.assigns, ~w(sort_key sort_direction name_filter)a)
      )

    ~L"""
    <button phx-click="previous">Previous Page</button>
    <button phx-click="next">Next Page</button>
    <a href="<%= download_path %>">Download as CSV</a>
    <table>
      <tr>
        <%= for {key, name} <- attributes do %>
          <th>
            <div phx-click="sort" phx-value-key="<%= key %>"><%= name %></div>
            <%= if key == "Player" do %>
              <form phx-change="filter">
                <input name="name" value="<%= @name_filter %>"/>
              </form>
            <% end %>
          </th>
        <% end %>
      </tr>
      <%= for player <- @players do %>
        <tr>
          <%= for {key, _} <- attributes do %>
            <td><%= player[key] %></td>
          <% end %>
        </tr>
      <% end %>
    </table>
    """
  end

  def mount(_params, _session, socket) do
    {:ok, update(socket, sort_key: "Player", sort_direction: :asc, name_filter: "", page: 0)}
  end

  # This sort is unstable. It doesn't try to preserve ordering from previous sorts.
  def handle_event("sort", %{"key" => sort_key}, socket) do
    sort_direction =
      if sort_key == socket.assigns.sort_key do
        if socket.assigns.sort_direction == :asc do
          :desc
        else
          :asc
        end
      else
        :asc
      end

    handle(socket, sort_key: sort_key, sort_direction: sort_direction)
  end

  def handle_event("filter", %{"name" => name_filter}, socket) do
    handle(socket, name_filter: name_filter, page: 0)
  end

  def handle_event("next", _, socket) do
    page = socket.assigns.page + 1

    players =
      list_players(
        socket.assigns.name_filter,
        socket.assigns.sort_key,
        socket.assigns.sort_direction,
        page
      )

    socket =
      case players do
        [] -> socket
        _ -> assign(socket, page: page, players: players)
      end

    {:noreply, socket}
  end

  def handle_event("previous", _, socket) do
    if socket.assigns.page > 0 do
      handle(socket, page: socket.assigns.page - 1)
    else
      {:noreply, socket}
    end
  end

  defp handle(socket, attrs) do
    {:noreply, update(socket, attrs)}
  end

  defp update(socket, attrs) do
    socket = socket |> assign(attrs)
    assign(socket, players: list_players(socket))
  end

  defp list_players(socket) do
    list_players(
      socket.assigns.name_filter,
      socket.assigns.sort_key,
      socket.assigns.sort_direction,
      socket.assigns.page
    )
  end

  defp list_players(name_filter, sort_key, sort_direction, page) do
    Players.list_players(sort_key, sort_direction, name_filter, @page_size, page * @page_size)
  end
end
