defmodule NflRushing.MixProject do
  use Mix.Project

  def project do
    [
      app: :nfl_rushing,
      version: "0.1.0",
      elixir: "~> 1.9.1",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      test_coverage: [tool: ExCoveralls]
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {NflRushing.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:credo, "~> 1.2.2", only: :test},
      {:csv, "~> 2.3.1"},
      {:excoveralls, "~> 0.12.2", only: :test},
      {:floki, "~> 0.25.0", only: :test},
      {:jason, "~> 1.1.2"},
      {:phoenix_live_reload, "~> 1.2.1", only: :dev},
      {:phoenix_live_view, "~> 0.7.0"},
      {:plug_cowboy, "~> 2.1.2"},
      {:sobelow, "~> 0.10.0", only: :test}
    ]
  end
end
