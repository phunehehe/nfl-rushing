#!/usr/bin/env bash
set -eufxo pipefail

npm install --prefix assets
mix deps.get
iex -S mix phx.server
