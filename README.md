# NFL Rushing

[![pipeline status](https://gitlab.com/phunehehe/nfl-rushing/badges/master/pipeline.svg)](https://gitlab.com/phunehehe/nfl-rushing/-/commits/master)
[![coverage report](https://gitlab.com/phunehehe/nfl-rushing/badges/master/coverage.svg)](https://phunehehe.gitlab.io/nfl-rushing/excoveralls.html)

This is a simple web app that displays some data about NFL players' rushing statistics using [Phoenix LiveView](https://hexdocs.pm/phoenix_live_view/Phoenix.LiveView.html). Shiny features:

- Click on a column header to sort by that column
- Type in the box below `Player's name` to filter by name
- Click `Next Page` or `Previous Page` to view more
- Click `Download as CSV` to export to CSV

System dependencies:

- [Elixir](https://elixir-lang.org/)
- [NPM](https://www.npmjs.com/)

If you use [Nix](https://nixos.org/nix/), use `nix-shell` to enter the development environment provided by [`shell.nix`](/shell.nix).

To start the development server execute [`run.bash`](/run.bash). It will be available at [`localhost:4000`](http://localhost:4000).

You can also use [Docker](https://www.docker.com/) to play with the app:

```
docker run --rm \
    --env SECRET_KEY_BASE=_INSERT_YOUR_FAVORITE_RANDOM_64_CHARACTERS_HERE_INSTEAD_OF_THIS_ \
    --publish 4000:4000 \
    registry.gitlab.com/phunehehe/nfl-rushing:latest
```

![Screenshot](/screenshot.png)

## Why LiveView?

Mainly, because it's a cool thing I want to try :)
But actually, it's built on good foundation ([Erlang](https://www.erlang.org/), Elixir, [Phoenix](https://www.phoenixframework.org/)) and it's killer feature is building big, real-time, interactive tables, which is exactly what this app does. It's also an interesting take on using (or pretending to use) the same language on both the backend and frontend.
